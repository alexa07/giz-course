#Exercise1
    # For any given number, if the number is divisible 3, return 'this'
    # If it's divisible by 5, return 'that'
    # If it's divisible by 3 and 5, return 'thisthat'
    # If divisible by neither, return the number
def thisthat(number):
    if number % 3 == 0 and number % 5 == 0 :
        return 'thisthat'
    elif number % 3 == 0:
        return 'this'
    elif number % 5 == 0:
        return 'that'
    else:
        return number

for num in range(1,20):
    print(thisthat(num))


#Exercise2
    # Given a list of numbers, return the average of the total.
def get_average(numlist):
    total = 0
    for n in numlist:
        total += n

    average = total/len(numlist)
    return average

x = [10, 20, 30]

print(get_average(x))


    #Exercise3
    # Given a set of dictionaries, where the key is a name of a family
    # member and the value is the age, return the name of the youngest member.

def youngest_member(family):
    youngest = 200
    youngest_name = ''

    for member in family:
        for name, age in member.items():
            if age < youngest:
                youngest = age
                youngest_name = name

    return youngest_name, youngest

x = [{'ana': 200},
    {'Alma': 85},
    {'eri': 36},
    {'loni': 12},
    {'anxhela': 50}]

print(youngest_member(x))


#Exercise4
        # Given a set of lists, where each list contains a pair of scores in
        # soccer matches between a local team and a foreign team, return
        # the total difference between goals scored and goals allowed.

def net_score(games):
    totAlb = 0
    totGrc = 0
    for goals in games :
        totAlb = totAlb + goals[0]
        totGrc = totGrc + goals[1]
    return totAlb - totGrc
x = [[3,1],[4,5],[4,2]]
print(net_score(x))



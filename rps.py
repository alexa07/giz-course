import random
human_points, comp_points = 0, 0
choices = ['Rock', 'Paper', 'Scissors' ]

while human_points < 3 and comp_points <3 :
    comp_choice = random.choice(choices)
    human_choice = input( 'Please make a selection : ')

    print(f'Computer selected {comp_choice}')

    print(f'You selected {human_choice}')


    #Tie
    if human_choice == comp_choice :
        print("Tie!")

    #Rock
    elif human_choice == "Rock" and comp_choice == "Paper":
        print("You Lose!")
        comp_points += 1
        print(f'{comp_points}:{human_points}')
    elif human_choice == "Rock" and comp_choice == "Scissors":
        print("You Win!")
        human_points += 1
        print(f'{comp_points}:{human_points}')

    #Paper
    elif human_choice == "Paper" and comp_choice == "Scissors":
        print("You Lose!")
        comp_points += 1
        print(f'{comp_points}:{human_points}')
    elif human_choice == "Paper" and comp_choice == "Rock":
        print("You Win!")
        human_points += 1
        print(f'{comp_points}:{human_points}')

    #Scissors
    elif human_choice == "Scissors" and comp_choice == "Rock":
        print("You Lose!")
        comp_points += 1
        print(f'{comp_points}:{human_points}')
    elif human_choice == "Scissors" and comp_choice == "Paper":
        print("You Win!")
        human_points += 1
        print(f'{comp_points}:{human_points}')
print('Game over')

if human_points > comp_points:
    print('Human wins')
else :
    print('Computer wins')

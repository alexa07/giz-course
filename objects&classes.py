class Car:

    def __init__(self, name):
        self.name = name
        self.passengers = []

class Person:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_in_car(self, target_car):
        target_car.passengers.append(self)

    def __repr__(self):
        return self.name


johnny = Person('Johnny', 30)
endri = Person('Endri', 25)
mycar = Car('Skandermaster')

johnny.get_in_car(mycar)
endri.get_in_car(mycar)

for p in mycar.passengers:
    print(p)
